displayAsUL(["hello", "world", "Kyiv", "Kharkiv", "Odesa", "Lviv"]);
displayAsUL(["1", "2", "3", "sea", "user", 23]);
displayAsUL(["Kharkiv", "Kyiv", ["Boryspil", "Irpin"], "Odesa", "Lviv", "Dnipro"]);

let deCountDown = document.createElement('p');
document.body.append(deCountDown);

let nSeconds = 5;
let timerID = setInterval(() => {deCountDown.textContent = `The page will be self cleared in ${nSeconds--} sec`;}, 1000);
setTimeout(() => {document.body.textContent = ''; clearInterval(timerID)}, (nSeconds + 1) * 1000);



function displayAsUL(aList, deParent = document.body) {
    let deUL = document.createElement('ul');
    for (const iter of aList) {
         let deLI = document.createElement('li');
         deLI.textContent = iter;
         deUL.append(deLI);
         if (Array.isArray(iter)) displayAsUL(iter, deLI);
    }

    deParent.append(deUL);
}

